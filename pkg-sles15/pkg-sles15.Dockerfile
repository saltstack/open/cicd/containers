FROM opensuse/leap:15

RUN zypper install -y \
    rpm-build \
    rpm-devel \
    rpmlint \
    patch \
    rpmdevtools \
    libffi-devel \
    ncurses-devel \
    gdbm-devel \
    readline-devel \
    zlib-devel \
    libuuid-devel \
    krb5-devel \
    gcc \
    gettext \
    git \
    make \
    zlib-devel \
    libsodium-devel \
    curl \
    libopenssl-devel \
    xmlsec1 \
    xmlsec1-devel \
    libxmlsec1-1 \
    libxmlsec1-openssl1 \
    ruby \
    systemd \
    sqlite3-devel \
    ruby-devel \
    xz \
    xz-devel \
    wget \
    && zypper install -y -t pattern devel_C_C++

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8
ENV PYVER '3.9.18'
ENV PYPRE '3.9'
ENV OPENSSLVER '3.1.4'

RUN curl -LO https://www.openssl.org/source/openssl-${OPENSSLVER}.tar.gz \
    && tar -xvf openssl-${OPENSSLVER}.tar.gz \
    && cd openssl-${OPENSSLVER} \
    && ./config shared --prefix=/usr/local/openssl31 --openssldir=/usr/local/openssl31 --libdir=lib \
    && make \
    && make install \
    && cd .. \
    && rm -rf openssl-${OPENSSLVER}.tar.gz openssl-${OPENSSLVER} \
    && ldconfig /usr/local/

RUN curl -LO https://www.python.org/ftp/python/${PYVER}/Python-${PYVER}.tar.xz \
    && tar xvf Python-${PYVER}.tar.xz \
    && cd Python-${PYVER} \
    && LDFLAGS="-Wl,-rpath=/usr/local/openssl31/lib" ./configure --with-openssl=/usr/local/openssl31 --with-system-ffi --enable-shared \
    && make  \
    && make install  \
    && cd ..  \
    && rm -rf Python-${PYVER}.tar.xz Python-${PYVER} \
    && ldconfig /usr/local/lib

RUN gem install --no-document fpm \
    && ln -s $(find /usr/bin/ -type f -name fpm\*) /usr/bin/fpm
