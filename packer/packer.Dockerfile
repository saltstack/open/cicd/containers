FROM alpine:3.16

ENV PACKER_VERSION 1.8.1

RUN apk --no-cache update && \
    apk --no-cache add aws-cli bash ca-certificates curl git groff jq less openssh-client py-pip unzip && \
    curl https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip \
    -o packer.zip && unzip packer.zip && mv packer /usr/local/bin && rm packer.zip && \
    mkdir -p ~/.packer.d/plugins && \
    wget https://github.com/wata727/packer-plugin-amazon-ami-management/releases/download/v1.2.0/packer-plugin-amazon-ami-management_v1.2.0_x5.0_linux_amd64.zip -P /tmp/ && \
    cd ~/.packer.d/plugins && unzip -j /tmp/packer-plugin-amazon-ami-management_*.zip -d ~/.packer.d/plugins && \
    mv packer-plugin-amazon-ami-management* packer-plugin-amazon-ami-management && \
    rm -rf /var/cache/apk/*
