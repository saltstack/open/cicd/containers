FROM centos:7

RUN yum install -y \
    vim-enhanced \
    gcc \
    rpm-build \
    rpm-devel \
    rpmlint \
    make \
    bash \
    coreutils \
    diffutils \
    patch \
    rpmdevtools \
    redhat-rpm-config \
    xz \
    libffi-devel \
    bzip2-devel \
    xz-devel \
    ncurses-devel \
    gdbm-devel \
    sqlite-devel \
    readline-devel \
    zlib-devel \
    libuuid-devel \
    ruby-devel \
    rubygems \
    wget \
    curl \
    xmlsec1-openssl \
    xmlsec1-devel \
    libtool-ltdl-devel \
    perl-IPC-Cmd \
    && echo -e '[saltstack-ius-git224]\nname=saltstack-ius-git224\nbaseurl=https://saltstack-ius-git224.s3.dualstack.us-west-2.amazonaws.com/$basearch/\ngpgcheck=0' > /etc/yum.repos.d/saltstack-ius-git224.repo \
    && yum groupinstall -y 'Development Tools' \
    && yum remove -y git* \
    && yum install -y git224 \
    && rm -rf /var/cache/yum/*

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8
ENV PYVER '3.9.18'
ENV PYPRE '3.9'
ENV OPENSSLVER '3.1.4'

RUN curl -LO https://www.openssl.org/source/openssl-${OPENSSLVER}.tar.gz \
    && tar -xvf openssl-${OPENSSLVER}.tar.gz \
    && cd openssl-${OPENSSLVER} \
    && ./config shared --prefix=/usr/local/openssl31 --openssldir=/usr/local/openssl31 --libdir=lib \
    && make \
    && make install \
    && cd .. \
    && rm -rf openssl-${OPENSSLVER}.tar.gz openssl-${OPENSSLVER} \
    && ldconfig /usr/local/

RUN curl -LO https://www.python.org/ftp/python/${PYVER}/Python-${PYVER}.tar.xz \
    && tar xvf Python-${PYVER}.tar.xz \
    && cd Python-${PYVER} \
    && LDFLAGS="-Wl,-rpath=/usr/local/openssl31/lib" ./configure --with-openssl=/usr/local/openssl31 --with-system-ffi --enable-shared \
    && make  \
    && make install  \
    && cd ..  \
    && rm -rf Python-${PYVER}.tar.xz Python-${PYVER} \
    && ldconfig /usr/local/lib

RUN gem install --no-document 'ffi:<1.13' 'fpm:=1.11.0'
