FROM ubuntu:22.04

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

RUN apt-get update \
    && apt-get install -y \
    gpg \
    vim \
    gcc \
    make \
    bash \
    coreutils \
    diffutils \
    apt-utils \
    patch \
    git \
    libffi-dev \
    libbz2-dev \
    xz-utils \
    libncurses5-dev \
    libncursesw5-dev \
    libgdbm-dev \
    libsqlite3-dev \
    libreadline-dev \
    zlib1g-dev \
    uuid-dev \
    devscripts \
    pbuilder \
    debhelper \
    ruby \
    ruby-dev \
    rubygems \
    wget \
    curl \
    libxml2-dev \
    libxmlsec1-openssl \
    libxmlsec1-dev \
    libltdl-dev \
    build-essential \
    bash-completion \
    libssl-dev \
    openssl \
    llvm \
    tk-dev \
    lzma \
    liblzma-dev \
    python3-openssl \
    python3-dev \
    python3-venv \
    python3-pip \
    && rm -rf /var/lib/apt/lists/*

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8
ENV PYVER '3.9.18'
ENV PYPRE '3.9'

RUN gem install --no-document fpm

RUN curl -LO https://www.python.org/ftp/python/${PYVER}/Python-${PYVER}.tar.xz \
    && tar xvf Python-${PYVER}.tar.xz \
    && cd Python-${PYVER} \
    && ./configure --with-system-ffi --enable-shared \
    && make  \
    && make install  \
    && cd ..  \
    && rm -rf Python-${PYVER}.tar.xz Python-${PYVER} \
    && ldconfig
